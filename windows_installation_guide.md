# Windows Installation Guide

This guide serves to install Aetersum, the Lunamalis server, on your Windows
computer. Related: the [Mac guide](mac_installation_guide.md) or the
[Linux Guide](linux_installation_guide.md).

1. First, you need to install Python. To do this, you need to download it from
 the [official website](https://www.python.org/downloads/release/python-352/).
1.
