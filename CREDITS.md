# Credits

More than one Python package that was not created under this project is used
within the project. These are:

* [yattag](http://www.yattag.org/) - [Lesser General Public License 2.1 (LGPL)](https://github.com/leforestier/yattag/tree/master/license) - [Github](https://github.com/leforestier/yattag)

 As this library is linked dynamically, the Lesser General Public License does
 not extend a copyleft-like effect to this project.

 No changes or modifications was made to yattag.

 The LGPL 2.1 file can be found in [licenses/lgpl-2.1.txt](licenses/lgpl-2.1.txt).

* [Tornado](http://www.tornadoweb.org/en/stable/) - [Apache License 2.0](https://github.com/tornadoweb/tornado/blob/master/LICENSE) - [Github](https://github.com/tornadoweb/tornado)

 No changes were made to Tornado.

* [passlib](https://passlib.readthedocs.io/en/stable/) - [3-clause BSD License](http://passlib.readthedocs.io/en/stable/copyright.html)

 License may be read in [licenses/3-clause_BSD-Passlib_-_Assurance_Technologies,_LLC.txt](licenses/3-clause_BSD-Passlib_-_Assurance_Technologies,_LLC.txt).

* [PyMySql](https://pymysql.readthedocs.io/en/latest/) - [MIT License](https://github.com/PyMySQL/PyMySQL/blob/master/LICENSE)

* [TOML, a Python library](https://github.com/uiri/toml) - [MIT License](https://github.com/uiri/toml/blob/master/LICENSE)

 License may be read in [licenses/mit-toml.txt](licences/mit-toml.txt)

 License may be read in [licenses/mit-PyMySQL_contributors.txt](licenses/mit-PyMySQL_contributors.txt)

A number of modules that are built into Python itself are used throughout the
project; their licenses can be found in their appropriate locations.

[Phaser CE (Community Edition)](https://github.com/photonstorm/phaser-ce)
version 2.11.0 is used in this project. It is licensed under the MIT license,
which can be [viewed here](licenses/mit-Photon_Storm_Ltd.txt).

This file was last updated on 2018-07-07.
