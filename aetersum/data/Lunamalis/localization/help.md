# Help

The directory structure looks like this:
```
	localization
		<language_name>
			<class_name>.toml
```

Example:
```
	localization
		en_US_POSIX
			system.toml
```

The language name should be taken from [CLDR (Unicode Common Locale Data Repository)](http://cldr.unicode.org/)
See the language names at [http://www.unicode.org/Public/cldr/33.1/core.zip/common/main](http://www.unicode.org/Public/cldr/33.1/)
for a list of language names.

See [here](http://web.archive.org/web/20180712210405/https://stackoverflow.com/questions/3724970/localized-country-names/3753717) for more information.
