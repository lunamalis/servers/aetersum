#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

host = 'localhost'
port = 8999

# If set to True, then only one player is allowed, and they get to instantly
# log in.
singleplayer_mode = True

# Seeing weird characters in your console? Turn this to False
enable_terminal_colors = True

# Minification is not possible, because I have not found a minifier library that
# supports ES2015 (ES6), even though it's been out for three years. Maybe that
# could be a new project to be worked on.
# expects: bool
# default: False
minify_javascript = False
# expects: bool
# default: False
force_minify_javascript = False

# This is a key used to secure HTTP cookies. Run `./start.sh new_cookie_key` to
# generate a new cookie key, and then paste that key here. If you leave it as
# `None`, it will generate a new key every time the server is started. If you
# change it to `False`, it will look for a file in
# `aetersum/secret_data/cookie_secret_key` and you whatever is in that file as
# the key. If it can't find a file even though you set it to `False`, it will
# just generate a new key every time the server is started.
# If you set it to a string, it will use that string.
# Warning: changing this value at any time will log everyone out of the game.
# *Everyone*.
# expects: None or bool(False) or str
# default: None
cookie_secret_key = False

universe = {
	# How wide and tall is your universe? 1,000,000 is the only tested value at
	# the moment.
	# default: 1000000
	# (1,000,000)
	'width': 1000000,
	'height': 1000000,
	# How many galaxies are there in one universe?
	'galaxy_count': 3,
	# Width and height of your galaxy.
	'galaxy_width': 10000,
	'galaxy_height': 10000,
	# How many stars are there in each galaxy in one universe?
	'systems_per_galaxy': 100,
	# How many "globular clusters" per galaxy? Globular clusters are dense
	# collections of stars, approximately 50-100 parsecs in diameter, containing
	# hundreds of thousands of stars. They are often old; many are more than 10
	# billion years of age, which is sometimes before even the galaxy they
	# reside in was formed. These structures are rare.
	'globular_clusters_per_galaxy': 3,
	# How many systems per globular cluster? Note: Will ignore systems_per_galaxy
	'systems_per_globular_cluster': 10,
	# How many "open clusters" per galaxy? These are spread out collection of
	# stars. In real life, they usually are 2-15 parsecs in diameter, and have
	# several thousand stars that all formed at about the same time. Old open
	# clusters are rare, because stars tend to move away from each other after
	# 200 million years. These structures are common.
	'open_clusters_per_galaxy': 5,
	# Number of systems per open cluster.
	'systems_per_open_cluster': 5,
	# Nebulae are star nurseries, where new stars are born. An example is the
	# Orion Nebula in our own galaxy. Primordial resources are more common here,
	# and it obscures most or all of the light from stars within them.
	'nebulae_per_galaxy': 3,
	# How many systems do a nebula spawn?
	'systems_per_nebula': 10,
	# Maximum number of satellites per system. As in, maximum number of planets.
	'max_satellites_per_primary': 1,
	# Max number of satellites per primary satellite. As in, maximum number of
	# moons per planet.
	'max_satellites_per_secondary': 1,
	# Max number of satellites per secondary satellite. As in, how many moons
	# can a moon of a planet have?
	'max_satellites_per_tertiary': 1
}
