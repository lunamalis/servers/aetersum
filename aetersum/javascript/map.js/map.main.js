let game = new Phaser.Game({
	width: 900,
	height: 700,
	renderer: Phaser.AUTO,
	name: 'Map - Lunamalis',
	parent: 'mainCanvas',
	state: {
		preload: alphaPreload,
		create: alphaCreate,
		update: alphaUpdate
	},
	fpsProblemNotifier: function(){console.warn('Could not keep up.');},
	onBlur: function(){game.paused = true;},
	onFocus: function(){game.paused = false;},
	onPause: function(){console.log('Paused!');},
	onResume: function(){console.log('Resumed!');},
	antialias: true,
	clearBeforeRender: true
});

// let ws = new WebSocket('ws://{{template_dict['config'].host}}:{{template_dict['config'].port}}/ws');

function alphaPreload(){
	console.log('Preloading');
	game.state.add('starmap', stateStarMap);
}
function alphaCreate(){
	console.log('Creating');
	// ws.onopen = function(){
	// 	console.log('opened');
	// 	game.state.start('starmap');
	// }
	// ws.onmessage = function(event){
	// 	console.log('messaged: ', event);
	// }
	// ws.onclose = function(){
	// 	lunamalis.wsClose();
	// }
	lunamalis.ws.connect(function(){
		console.log(arguments);
		game.state.start('starmap')
	});

}
function alphaUpdate(){}
