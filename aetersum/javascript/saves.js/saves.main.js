lunamalis.ws.connect(function(){
	main();
});

function main(){
	// lunamalis.ws.ws.onmessage = function(event){
	// 	if event.data =
	// };
	lunamalis.ws.ws.onmessage = function(event){
		let response = lunamalis.ws.onMessage(event);
		if(response[0]==='j'){
			let jsonResponse = JSON.parse(response[1])
			if(jsonResponse.hasOwnProperty('listSaves')){
				listSaves(jsonResponse.listSaves)
			}
		}
	}
	lunamalis.ws.send('alistSaves');
}

function listSaves(json){
	let saveString = '';
	let saveI = 0;
	let saveLength = json.length;
	for(;saveI < saveLength; saveI++){
		saveString += (
			'<tr><td>'
			+
			json[saveI].id
			+
			'</td><td>'
			+
			json[saveI].name
			+
			'</td><td><button class="selectBtn">Select</button></td></tr>'
		);
	}
	let saveList = document.getElementById('saveList');
	saveList.innerHTML = (
		'<tr><th>Id</th><th>Name</th><th>Select</th></tr>'
		+
		saveString
	);
	let lsget = lunamalis.ls.get();
	if(lsget.hasOwnProperty('saveN')){
		try{
			saveList.children[0].children[Number(lsget.saveN)+1].classList.add('selected');
		}
		catch(e){
			saveList.innerHTML = 'You don\'t have any save games!'
		}
	}
	saveList.addEventListener('click', function(e){
		if(e.target && e.target.classList && e.target.classList.contains('selectBtn')){
			tableSelectBtn(e);
		}
	});
	let btns = document.getElementsByClassName('selectBtn');
	let btnsI = 0;
	let btnsLength = btns.length;
	for(;btnsI<btnsLength;btnsI++){
		btns[btnsI].addEventListener('click', tableSelectBtn);
	}

	document.getElementById('newSave').addEventListener('click', function(e){
		e.target.parentNode.addEventListener('click', saveSave);
		e.target.parentNode.innerHTML = '<form><label>Name of Save *Required</label><input id="saveName" type="text" required="required"><button id="saveSave">Submit</button></form>'
	});
}

function tableSelectBtn(e){
	// First, remove all previous selected elements.
	let selectedEls = e.target.parentNode.parentNode.parentNode.querySelectorAll(
		'.selected'
	);
	for(let selectedElI = 0; selectedElI<selectedEls.length; selectedElI++){
		selectedEls[selectedElI].classList.remove('selected');
	}
	e.target.parentNode.parentNode.classList.add('selected');
	lunamalis.ls.update({
		saveN: e.target.parentNode.parentNode.children[0].innerHTML
	});
	lunamalis.ws.sendA(['setSave', e.target.parentNode.parentNode.children[0].innerHTML])
}

function saveSave(e){
	e.preventDefault();
	if(e.target && e.target.id === 'saveSave'){
		let value = e.target.parentNode.children[1].value;
		if(value === '' || value === null || value === undefined){
			e.target.parentNode.children[0].innerHTML = 'Name of Save *REQUIRED';
		}
		else{
			// lunamalis.ws.send('anewSave '+e.target.parentNode.children[1].value);
			lunamalis.ws.sendA(['newSave', e.target.parentNode.children[1].value]);
			// Display it on a table.
			let tableTarget = e.target.parentNode.parentNode.parentNode.children[2].children[0];
			tableTarget.innerHTML += (
				'<tr><td>'
				+
				(tableTarget.children.length-1)
				+
				'</td><td>'
				+
				e.target.parentNode.children[1].value
				+
				'</td><td><button class="selectBtn">Select</button></td></tr>'
			);
			tableTarget.children[tableTarget.children[0].children.length]
				.children[2].children[0].addEventListener('click', tableSelectBtn)
		}
	}
};
