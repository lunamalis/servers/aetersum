lunamalis.c.performance_begin = performance.now();

window.onbeforeunload = function(){
	if(lunamalis.stop_reload){
		return "Are you sure you want to leave?";
	}
	// lunamalis.ws.ws.onc
};
window.onunload = function(){
	lunamalis.ws.ws.onclose = function(){};
	lunamalis.ws.ws.close();
};
lunamalis.stop_reload = true;
if(
	{{
		template_dict['pages_allowed_to_reload']
	}}
){
	lunamalis.stop_reload = false;
}

lunamalis.check = function(json){
	if(typeof json.realuser !== 'undefined'){
		alert('You will need to login to continue. You will be redirected to the login page.');
		window.location = 'login';
	}
}

lunamalis.ws = {
	ws: null,
	aReplace: [
		[' ', '=SPACE=']
	],
	connect: function(do_on_connect){
		this.do_on_connect = do_on_connect || function(){};
		this.ws = new WebSocket(
			'ws://{{template_dict['config'].host}}:{{template_dict['config'].port}}/ws'
		);
		this.ws.onopen = this.onConnect;
		this.ws.onmessage = function(){};
		this.ws.onclose = this.onClose;
	},
	onConnect: function(){
		lunamalis.ws.do_on_connect();
	},
	/**
	 * Fires whenever we receive a message from the server.
	 * @param  {[type]} event [description]
	 * @return {[type]}       [description]
	 */
	onMessage: function(event){
		// console.warn(event.data.substring(1));
		// Get the data type.
		let evtType = event.data.charAt(0);
		// Get everything after it the data type.
		let evtData = event.data.substring(1);
		return [
			event.data.charAt(0),
			event.data.substring(1)
		]
	},
	onClose: function(){
		console.log('Closed: ', arguments);
	},
	send: function(msg){
		return this.ws.send(msg);
	},
	sendA: function(msgs){
		for(let msgsI = 0; msgsI<msgs.length; msgsI++){
			msgs[msgsI] = msgs[msgsI].replace(/=/g,'=_');
			for(let replaceI = 0; replaceI<this.aReplace.length; replaceI++){
				// console.log(this.aReplace);
				msgs[msgsI] = msgs[msgsI].replace(new RegExp(this.aReplace[replaceI][0], 'g'), this.aReplace[replaceI][1]);
			}
			msgs[msgsI] = msgs[msgsI].replace(/=_/g, '=E=');
		}
		return this.ws.send('a'+msgs.join(' '));
	}
}

lunamalis.wsClose = function(code, reason){
	document.getElementById('wsStatus').innerHTML = 'Connection lost.'
};

lunamalis.mergeObj = function(first, second){
	let returnObj = {};
	for(let attrN in first){returnObj[attrN] = first[attrN]}
	for(let attrN in second){returnObj[attrN] = second[attrN]}
	return returnObj;
}

/**
 * Wrapper for the localStorage object.
 * @type {Object}
 */
lunamalis.ls = {
	/**
	 * Gets the data.
	 * @return {string} String gotten from localStorage.
	 */
	get: function(){
		let returner = {};
		try{
			returner = JSON.parse(
				localStorage.getItem('{{template_dict['localstorage_name_attr']}}')
			);
		}
		catch(e){
			return {};
		}
		if(returner === null){
			return {};
		}
		return returner;
	},
	/**
	 * Sets the data.
	 * @param  {Object} what Object to be placed in.
	 * @return {undefined}   localStorage never returns anything.
	 */
	set: function(what){
		localStorage.setItem(
			'{{template_dict['localstorage_name_attr']}}',
			JSON.stringify(what)
		);
	},

	/**
	 * "Clears" the value. Note that localStorage.clear() clears all values.
	 * @return {undefined} It doesn't return anything.
	 */
	empty: function(){
		lunamalis.ls.set('{}');
	},

	/**
	 * Changes the value.
	 * @param  {Object} obj Object to add to the value.
	 * @return {undefined}  Undefined
	 */
	update: function(obj){
		lunamalis.ls.set(
			lunamalis.mergeObj(
				lunamalis.ls.get(),
				obj
			)
		);
	}
};

Object.freeze(lunamalis.c);
