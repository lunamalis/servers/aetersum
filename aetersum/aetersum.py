#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import os
import sys
import importlib
import datetime
import time
import logging
import threading
import base64

import logger_main
import cli_parser
import options.version
import options.tornado_settings
from pages.main import PAGES
http_error_handlers = {
	'404': importlib.import_module('pages.http_errors.404_Not_Found')
}
import pages.index
import pages.login
import pages.register
import pages.ws
import pages.css
import pages.js.main
import pages.js.phaser
import pages.map
import pages.js.map_js
import pages.saves
import pages.js.saves_js
import config

import mod_manager
import asyncio
import tornado.ioloop
import tornado.web
import tornado.websocket

app = None
cwd = os.getcwd()
logger = logger_main.main()
args = cli_parser.main()

template_dict = {
	'config': config,
	'PAGES': PAGES,
	'pages_allowed_to_reload': []
}
tmp_pages_allowed_to_reload_list = []
for page in PAGES.values():
	if len(page) > 1:
		if 'allow_reload' in page[1]:
			tmp_pages_allowed_to_reload_list.append('lunamalis.c.page==='+str(page[0])+'||')

template_dict['pages_allowed_to_reload'] = ''.join(tmp_pages_allowed_to_reload_list)[:-2]

template_dict['localstorage_name_attr'] = (
	'aetersum' + options.version.aetersum_version + '-lunamalis' + \
	options.version.lunamalis_version + '-' + str(config.port)
)

template_dict['cookie_name'] = (
	'aetersum' + options.version.aetersum_version + '-lunamalis' + \
	options.version.lunamalis_version + '-' + str(config.port)
)

cookie_secret_key = config.cookie_secret_key
if cookie_secret_key == False:
	try:
		with open(os.path.join('aetersum','secret_data','cookie_secret_key')) as cookie_secret_key_f:
			cookie_secret_key = cookie_secret_key_f.read().strip()
	except FileNotFoundError:
		print('aetersum'+os.sep+'secret_data'+os.sep+'cookie_secret_key not found! Generating new key.')
		# Slow, but you shouldn't use this anyway.
		cookie_secret_key = base64.b64encode(os.urandom(50)).decode('ascii')
elif cookie_secret_key is None:
	print('Generating a new cookie_secret_key. This could take a while.')
	# Slow, but you shouldn't use this anyway.
	cookie_secret_key = base64.b64encode(os.urandom(50)).decode('ascii')


phaser_path = options.version.phaser_version
if options.version.phaser_ce:
	phaser_path = 'ce' + os.sep + phaser_path
phaser_path = os.path.join('aetersum','static','phaser',phaser_path,'phaser.min.js')
phaser_map_path = phaser_path[:-13] + 'phaser.map'

tornado_settings_end = options.tornado_settings.settings
tornado_settings_end.update({
	'default_handler_class': http_error_handlers['404'].NotFoundHandler,
	'cookie_secret': cookie_secret_key
})

class MainHandler(tornado.web.RequestHandler):
	# @tornado.web.asynchronous stops it from automatically closing the
	# connection at the end of the function and lets us do it ourselves.
	@tornado.web.asynchronous
	def get(self):
		self.write("""
		test
		""")
		self.finish()


def shutdown_tornado():
	if hasattr(config, 'shutting_down'):
		print('Already shutting down')
	else:
		config.shutting_down = True
		print('Initiating total shutdown sequence.')
		clients = pages.ws.clients
		print('Number of clients to get rid of: {}'.format(len(clients)))
		for client in clients:
			client['object'].close(code=1001, reason='Server closed')
		print('Stopping instance of Tornado.')
		ioloop = tornado.ioloop.IOLoop.instance()
		ioloop.add_callback(ioloop.stop)
		print('Shutdown finished.')

def main():
	# Load all mods.
	mod_m = mod_manager.ModManager(
		logger=logger,
		shutdown=shutdown_tornado
	)
	logger.info('Today is {date}'.format(date=datetime.datetime.today()))
	logger.info('The seconds since the UTC epoch is {epoch}.'.format(
		epoch=time.time())
	)
	logger.info('Welcome to Aetersum.')
	mod_m.load_all()
	webapp = tornado.web.Application(
		[
			(r"/", pages.index.IndexHandler, {'page': PAGES['INDEX'][0]}),
			(r"/ws/*", pages.ws.WebSocketHandler, {'mod_m': mod_m}),
			(r"/index", pages.index.IndexHandler, {'page': PAGES['INDEX'][0]}),
			(r"/login", pages.login.LoginHandler, {'page': PAGES['LOGIN'][0]}),
			(r"/register", pages.register.RegisterHandler, {'page': PAGES['REGISTER'][0]}),
			(r"/css/main", pages.css.CSSHandler, template_dict),
			(r"/js/main", pages.js.main.JavascriptHandler, template_dict),
			(r"/js/phaser", pages.js.phaser.PhaserHandler, {'path': phaser_path}),
			(r"/js/phaser.map", pages.js.phaser.PhaserHandler, {'path': phaser_map_path}),
			(r"/map", pages.map.MapHandler, {'page': PAGES['MAP'][0]}),
			(r"/js/map", pages.js.map_js.JavascriptHandler, template_dict),
			(r"/saves", pages.saves.SavesHandler, {'page': PAGES['SAVES'][0]}),
			(r"/js/saves", pages.js.saves_js.JavascriptHandler, template_dict)
		],
		**tornado_settings_end
	)
	webapp.listen(config.port)
	try:
		tornado.ioloop.IOLoop.instance().start()
	except KeyboardInterrupt:
		print('\nKeyboardInterrupt: Shutting down gracefully.')
		print('Press ^C again to skip graceful shutdown and quit immediately.')
		print('Note that this has potential to be dangerous.')
		shutdown_tornado()
		os._exit(0)

if __name__ == "__main__":
	main()
