#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import config
import tornado.web
import yattag
import re
from passlib.apps import custom_app_context as passlib_pwd_context

import mysql_handler
import html_handler

class RegisterHandler(tornado.web.RequestHandler):
	def initialize(self, **kwargs):
		self.page = kwargs['page']
	# @tornado.web.asynchronous stops it from automatically closing the
	# connection at the end of the function and lets us do it ourselves using
	# self.finish()
	@tornado.web.asynchronous
	def get(self):
		write_end = html_handler.Head(page=self.page).main()
		doc, tag, text, line = yattag.Doc().ttl()
		with tag('body'):
			body = html_handler.Body()
			doc.asis(body.header())
			if config.singleplayer_mode:
				with tag('p'):
					text('Single player mode is enabled! Only one player can')
					text(' play at a time. You have already been logged in.')
				with tag('script'):
					text('window.location.replace("login");')
			else:
				with tag('form', id="registerForm", method="POST"):
					with tag('h3'):
						text('Register')
					with tag('div', klass="input"):
						with tag('label', id="registerUsernameLabel"):
							text('Username*')
						with tag('input', id="registerUsername", name="username", required="required"):
							text('')
					doc.stag('br')
					with tag('p'):
						text('You are not required to give an email, but doing so\
						would allow you to reset your password.')
					with tag('div', klass='input'):
						with tag('label', id="registerEmailLabel"):
							text('Email')
						with tag('input', id="registerEmail", name="email", type="email"):
							text('')
					doc.stag('br')
					with tag('div', klass="input"):
						with tag('label', id="registerEmailConfirmLabel"):
							text('Confirm email')
						with tag('input', id="registerEmailConfirm", name="emailc", type="email"):
							text('')
					doc.stag('br')
					with tag('div', klass="input"):
						with tag('label', id="registerPasswordLabel"):
							text('Password*')
						with tag('input', id="registerPassword", name="pw", type="password", required="required"):
							text('')
					doc.stag('br')
					with tag('div', klass="input"):
						with tag('label', id="registerPasswordConfirmLabel"):
							text('Confirm password*')
						with tag('input', id="registerPasswordConfirm", name="pwc", type="password", required="required"):
							text('')
					doc.stag('br')
					with tag('button', id="registerSubmit", klass="formSubmit", type="submit", name="register", value="1"):
						text('Register')
					with tag('p'):
						text('* Required')
			doc.asis(body.footer())
		write_end += doc.getvalue()
		self.write(html_handler.HTMLWrapper.main(write_end))
		self.finish()

	@tornado.web.asynchronous
	def post(self):
		if config.singleplayer_mode:
			with tag('body'):
				with tag('p'):
					text('Singleplayer mode active; registering accounts now')
					text('allowed. Redirecting you.')
				with tag('script'):
					text('window.location.replace("login");')
		else:
			end_user_email = None
			email = self.get_argument('email', default=None)
			emailc = self.get_argument('emailc', default=None)
			username = self.get_argument('username', default=None)
			password = self.get_argument('pw', default=None)
			passwordc = self.get_argument('pwc', default=None)
			stop_sql = False
			write_end = html_handler.Head(page=self.page).main()
			doc, tag, text, line = yattag.Doc().ttl()
			with tag('body'):
				# Email is optional.
				if email:
					if email == emailc:
						# Email validation is tough, but all emails should at least
						# have an `@` symbol in them. We are not emailing anybody on
						# on our local network (`invalid-email` is a valid email
						# address, but only on the local network).
						if '@' in email:
							end_user_email = email
						else:
							with tag('p'):
								text('Your email should at least have 1 ')
								with tag('code'):
									text('@')
								text(' symbol in it.')
							stop_sql = True
					else:
						# The email and emailc values are different.
						with tag('p'):
							text('If you are going to provide an email, make sure\
							that the email input and email confirmation input are\
							both exactly the same.')
						stop_sql = True
				# Continue.
				if username:
					# pattern = re.compile('[\W_]+', re.UNICODE)
					pattern = re.compile('^[a-zA-Z0-9-_]+', re.UNICODE)
					# This will filter it to nothing. For example, 'test-_o' turns
					# to '' which is okay, but 'test-_o)' turns to ')' which is not
					# okay.
					if pattern.sub('', username) == '':
						if password and passwordc:
							if password == passwordc:
								with tag('p'):
									text('Congrats!')
							else:
								with tag('p'):
									text('The password and password confirmation\
									input must match.')
								stop_sql = True
						else:
							with tag('p'):
								text('You must submit a password and confirm it.')
							stop_sql = True
					else:
						with tag('p'):
							text('Your username can only have letters, numbers, \
							underscores, and dashes within it. (Regex used is ')
							with tag('code'):
								with tag('span'):
									text('^[a-zA-Z0-9-_]+')
							text(' )')
						stop_sql = True
				if not stop_sql:
					hashed_password = passlib_pwd_context.hash(password)
					connection = mysql_handler.connect()
					try:
						with connection.cursor() as cursor:
							# First, we should check to see if the required username
							# is taken or not.
							# We COULD do something like Discord does, and allow
							# 9999 duplicates of a username, being pingable with
							# `username@user_id`, but that isn't necessary. It could
							# be a thing to do.
							cursor.execute(
								"""
									SELECT `user_id` FROM `user_primary`
									WHERE `user_name` = %s
								""",
								(username)
							)
							result = cursor.fetchone()
							if result != None:
								with tag('p'):
									with tag('a', href="register"):
										text('That username is taken. Please choose\
										another.')
							else:
								cursor.execute(
									"""
										INSERT INTO `user_primary` (
											`user_name`,
											`user_email`,
											`user_password`
										)
										VALUES (
											%s,
											%s,
											%s
										)
									""",
									(
										username,
										email,
										hashed_password
									)
								)
								connection.commit()
								with tag('p'):
									with tag('a', href="login"):
										text('You have registered! Now, you must\
										login.')
					finally:
						connection.close()

			write_end += doc.getvalue()
			self.write(html_handler.HTMLWrapper.main(write_end))
			self.finish()


def main():
	# TODO: Print some debug information if called directly?
	pass

if __name__ == '__main__':
	main()
