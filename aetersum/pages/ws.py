#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import config
import options.version
import mysql_handler

import os
from passlib.apps import custom_app_context as passlib_pwd_context
import tornado.websocket
import json
import shutil
import importlib


clients = []

class WebSocketHandler(tornado.websocket.WebSocketHandler):
	def initialize(self, **kwargs):
		self.mod_m = kwargs['mod_m']

	def open(self, *args):

		client_id = len(clients)
		self.id = client_id
		session_passed = False
		user_data = None
		if config.singleplayer_mode:
			session_passed = True
			user_data = {
				'user_id': 1,
				'user_name': 'Alice',
				'user_email': '',
				'user_password': '',
				'user_tmp_session_id': 0
			}
		else:
			# First, we need to check their cookie to see if they are logged in.
			cookie = self.get_secure_cookie(
				'aetersum' + options.version.aetersum_version +\
				'-lunamalis' + \
				options.version.lunamalis_version + '-' + \
				str(config.port)
			)
			cookie_end = cookie.decode('utf-8')
			# Connect to database to check to see if the username and password are
			# correct.
			# This variable is any integer from 0 to 3.
			session_passed = False
			connection = mysql_handler.connect()
			try:
				with connection.cursor() as cursor:
					cursor.execute(
						"""
							SELECT * FROM `user_primary`
							WHERE `user_tmp_session_id` = %s;
						""",
						(cookie_end)
					)
					user_data = cursor.fetchone()
					if user_data != None:
						session_passed = True
						# Prevent duplicate entries.
						for client in clients:
							if 'user_id' in client['user_data']:
								if client['user_data']['user_id'] == user_data['user_id']:
									session_passed = False
			finally:
				connection.close()

		if not session_passed:
			print('Bad cookie attempt; closing web socket.')
			self.close(code=1001, reason='badCookie')
		else:
			print('Client {id} joined'.format(id=client_id))
			self.stream.set_nodelay(True)
			main_json_dir = os.path.join(
				os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
				'storage',
				str(user_data['user_id'])
			)
			os.makedirs(main_json_dir, exist_ok=True)
			main_json_path = os.path.join(
				main_json_dir,
				'main.json'
			)
			if not os.path.isfile(main_json_path):
				try:
					with open(main_json_path, 'x') as f:
						f.write(json.dumps({
							'saves': []
						}))
				except FileExistsError:  # Race condition.
					pass
			with open(
				main_json_path
			) as main_json_f:
				self.json = {
					'main': json.loads(main_json_f.read())
				}
			clients.append({
				'id': client_id,
				'object': self,
				'user_data': user_data,
				'json': self.json
			})

	def on_message(self, message):
		# print('Got message ', message)
		if self.id <= len(clients):
			message_type = str(message)[0]
			# message_return = str(message)[1]
			# message_data = str(message)[2:]
			message_data = str(message)[1:]
			if message_type == 'a':
				message_cmds = message_data.split(' ')
				if message_cmds[0] == 'setSave':
					# Make sure that the save actually exists.
					if int(message_cmds[1]) <= len(clients[self.id]['json']['main']['saves']):
						clients[self.id]['current_save'] = int(message_cmds[1])

				elif message_cmds[0] == 'listSaves':
					end_json = {'listSaves': []}
					for save in clients[self.id]['json']['main']['saves']:
						end_json['listSaves'].append(save)
					self.write_message('j'+str(json.dumps(end_json)))

				elif message_cmds[0] == 'newSave':
					client_main_json = None
					client_main_json_path = os.path.join(
						os.getcwd(),
						'aetersum',
						'storage',
						str(clients[self.id]['user_data']['user_id']),
						'main.json'
					)
					with open(client_main_json_path, 'r') as client_main_json_f:
						client_main_json = json.loads(client_main_json_f.read())
						client_main_json['saves'].append({
							'id': len(client_main_json['saves']),
							'name': self.a_string_replace(message_cmds[1])
						})
					with open(client_main_json_path, 'w') as client_main_json_f:
						client_main_json_f.write(json.dumps(client_main_json))
					clients[self.id]['json']['main'] = client_main_json
					self.write_message('a'+str('1'))

				elif message_cmds[0] == 'getMap':
					self.write_message(self.get_map())

				elif message_cmds[0] == 'gotoSystem':
					# They want to go a specific system.
					# First, we need to determine if they even can.
					self.get_universe()
					allowed = self.univ.travel_to_system_allowed(
						first_system=self.univ.get_current_system_id(),
						second_system=int(message_cmds[1])
					)
					if allowed:
						self.univ.travel_to_system(
							to_system=int(message_cmds[1]),
							from_system=self.univ.get_current_system_id(),
							fleet_id=0  # Fleet id is 0
						)
						self.write_message(self.get_map())
					else:
						self.write_message('aNoMove')

				# Cheats are useful for debugging.
				elif message_cmds[0] == 'cheat':
					if 'cheats' in self.json['main']:
						if self.json['main']['cheats']:
							print(
								'Cheater {clientid} using command '
								'{cheatcommand}'
								.format(
									clientid=self.id,
									cheatcommand=message_cmds
								)
							)
							if message_cmds[1] == 'test':
								self.write_message('a1')
							elif message_cmds[1] == 'gotoFactionHomeSystem':
								self.get_universe()
								for system in self.univ.univ['systems']:
									# print(system)
									if system.get('home', -1) != -1:
										print('Found a home')
										if system['home'] == int(message_cmds[2]):
											self.univ.tp_to_system(
												fleet_arg=int(message_cmds[2]),
												to_system=system['id']
											)
											self.write_message(self.get_map())
					else:
						self.write_message('ano')

	def on_close(self):
		if self.id <= len(clients):
			self.save_universe()
			print('Client {id_goodbye} left'.format(id_goodbye=self.id))
			del clients[self.id]
			# Save the universe.
		else:
			print('Can not close web socket id {id}! Length is {length}'.format(
				id=self.id,
				length=len(clients))
			)

	# This works.
	# It does replacement to make sure a thing works in web sockets.
	def a_string_replace(self, text):
		text_replace_dict = {
			'=SPACE=': ' '
		}
		text = text.replace('=E=', '= ')
		for key, value in text_replace_dict.items():
			text = text.replace(key, value)
		text = text.replace('= ', '=')
		return text

	def get_universe(self, redo=False):

		if (redo) or (not hasattr(self, 'univ')):
			self.univ = None

			map_dir = os.path.join(
				os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
				'storage',
				str(clients[self.id]['user_data']['user_id']),
				'saves',
				str(clients[self.id]['current_save'])
			)
			map_path = os.path.join(map_dir, 'map.json')
			os.makedirs(os.path.dirname(map_path), exist_ok=True)

			universe_handler = importlib.import_module('universe_handler')
			universe = None
			try:
				with open(map_path, 'r') as map_f:
					map_json = json.loads(map_f.read())
					universe = universe_handler.create(map_json)
			except FileNotFoundError:
				# No map? We need to generate a new one.
				# universe_handler = importlib.import_module('universe_handler')
				universe = universe_handler.generate(
					user_id=clients[self.id]['user_data']['user_id'],
					user_save_id=clients[self.id]['current_save'],
					mod_m=self.mod_m
				)
				map_end = universe.get_data()
				with open(map_path, 'w') as map_f:
					map_f.write(str(map_end))

			self.univ = universe
		return self.univ

	def save_universe(self):
		'''Saves the universe, writing to it's JSON file.'''
		map_dir = os.path.join(
			os.path.dirname(os.path.dirname(os.path.realpath(__file__))),
			'storage',
			str(clients[self.id]['user_data']['user_id']),
			'saves',
			str(clients[self.id]['current_save'])
		)
		map_path = os.path.join(map_dir, 'map.json')
		os.makedirs(os.path.dirname(map_path), exist_ok=True)
		if hasattr(self, 'univ'):
			# No need for a try/except as we are writing the file.
			with open(map_path, 'w') as map_f:
				map_f.write(self.univ.get_data())


	def get_map(self):
		self.get_universe()
		current_system = self.univ.get_current_system(fleet=0)
		current_system_id = self.univ.get_current_system_id(fleet=0)
		map_end = self.univ.get_systems_near(
			x=current_system['point'][0],
			y=current_system['point'][1],
			dist=1000
		)
		map_end = 'j'+str(json.dumps({
			'nearbySystems': map_end,
			'currentSystemId': current_system_id
		}))
		# print(map_end)
		return map_end
