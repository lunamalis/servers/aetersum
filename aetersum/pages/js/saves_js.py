#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import os
import tornado.template
import tornado.web

import javascript_handler

class JavascriptHandler(tornado.web.RequestHandler):
	javascript_files = [
		{
			'from': [
				os.sep+'saves.js'+os.sep+'saves.page_check.js',
				os.sep+'saves.js'+os.sep+'saves.main.js',
			]
		}
	]
	def initialize(self, **template_dict):
		self.template_dict = template_dict
	# @tornado.web.asynchronous stops it from automatically closing the
	# connection at the end of the function and lets us do it ourselves using
	# self.finish()
	@tornado.web.asynchronous
	def get(self):
		self.set_header("Content-Type", 'application/javascript; charset="utf-8"')
		self.write(javascript_handler.JSCompiler(self.javascript_files, template_dict=self.template_dict).do())
		self.finish()

def main():
	# TODO: Print some debug information if called directly?
	pass

if __name__ == '__main__':
	main()
