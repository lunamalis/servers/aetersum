#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import os
import tornado.template
import tornado.web

import javascript_handler

class JavascriptHandler(tornado.web.RequestHandler):
	javascript_files = [
		{
			'from': [
				os.sep+'map.js'+os.sep+'map.page_check.js',
				os.sep+'map.js'+os.sep+'states'+os.sep+'stateStarMap.js',
				os.sep+'map.js'+os.sep+'map.main.js',
				os.sep+'map.js'+os.sep+'mothership.js'
			]
		}
	]
	def initialize(self, **template_dict):
		self.template_dict = template_dict
	# @tornado.web.asynchronous stops it from automatically closing the
	# connection at the end of the function and lets us do it ourselves using
	# self.finish()
	@tornado.web.asynchronous
	def get(self):
		# write_end = []
		# upper_path = os.path.join(
		# 	os.getcwd(),
		# 	'aetersum/javascript/upper.js'
		# )
		# lower_path = os.path.join(
		# 	os.getcwd(),
		# 	'aetersum/javascript/lower.js'
		# )
		# for js_file in self.javascript_files:
		# 	if 'no_upper' not in js_file:
		# 		with open(
		# 			upper_path,
		# 			'r'
		# 		) as upper_f:
		# 			# print(str(**self.template_dict))
		# 			upper_f_template = tornado.template.Template(upper_f.read())
		# 			write_end.append(
		# 				upper_f_template.generate(template_dict=self.template_dict)
		# 			)
		#
		# 	for js_file_individual in js_file['from']:
		# 		file_get_from = os.path.join(
		# 			os.getcwd(),
		# 			'aetersum/javascript' + js_file_individual
		# 		)
		# 		with open(file_get_from, 'r') as file_got:
		# 			tmp_template = tornado.template.Template(file_got.read())
		# 			write_end.append(
		# 				tmp_template.generate(template_dict=self.template_dict)
		# 			)
		#
		# 	if 'no_lower' not in js_file:
		# 		with open(
		# 			lower_path,
		# 			'r'
		# 		) as lower_f:
		# 			lower_f_template = tornado.template.Template(lower_f.read())
		# 			write_end.append(
		# 				lower_f_template.generate(template_dict=self.template_dict)
		# 			)

		self.set_header("Content-Type", 'application/javascript; charset="utf-8"')
		# It gets byte objects, not string.
		# self.write(b''.join(write_end))
		self.write(javascript_handler.JSCompiler(self.javascript_files, template_dict=self.template_dict).do())
		self.finish()

def main():
	# TODO: Print some debug information if called directly?
	pass

if __name__ == '__main__':
	main()
