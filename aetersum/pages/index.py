#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import tornado.web
import yattag

import html_handler

class IndexHandler(tornado.web.RequestHandler):
	def initialize(self, **kwargs):
		self.page = kwargs['page']
	# @tornado.web.asynchronous stops it from automatically closing the
	# connection at the end of the function and lets us do it ourselves using
	# self.finish()
	@tornado.web.asynchronous
	def get(self):
		write_end = html_handler.Head(page=self.page).main()
		doc, tag, text, line = yattag.Doc().ttl()
		with tag('body'):
			body = html_handler.Body()
			doc.asis(body.header())
			with tag('p'):
				text('To play Lunamalis, you will first have to ')
				with tag('a', href="login"):
					text('log in')
				text(' or ')
				with tag('a', href="register"):
					text('register')
				text('.')
			with tag('p'):
				with tag('a', href="map"):
					text('Map')
			doc.asis(body.footer())
		write_end += doc.getvalue()
		self.write(html_handler.HTMLWrapper.main(write_end))
		self.finish()

def main():
	# TODO: Print some debug information if called directly?
	pass

if __name__ == '__main__':
	main()
