#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

PAGES = {
	'INDEX': [1, {'allow_reload': True}],
	'MAIN_JS': [2],
	'MAP': [3],
	'MAP_JS': [4],
	'API': [4],
	'LOGIN': [5, {'allow_reload': True}],
	'REGISTER': [6, {'allow_reload': True}],
	'SAVES': [7]
}
