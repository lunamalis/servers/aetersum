#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import tornado.web
import yattag
import random

import config
import options.version
import mysql_handler
import html_handler
from passlib.apps import custom_app_context as passlib_pwd_context

class LoginHandler(tornado.web.RequestHandler):
	def initialize(self, **kwargs):
		self.page = kwargs['page']
	# @tornado.web.asynchronous stops it from automatically closing the
	# connection at the end of the function and lets us do it ourselves using
	# self.finish()
	@tornado.web.asynchronous
	def get(self):
		write_end = html_handler.Head(page=self.page).main()
		doc, tag, text, line = yattag.Doc().ttl()
		with tag('body'):
			body = html_handler.Body()
			doc.asis(body.header())
			# with tag('p'):
			# 	if cookie_is_real:
			# 		text('You are already logged in.')
			# 	else:
			# 		text('You are not logged in.')
			if config.singleplayer_mode:
				with tag('p'):
					text('Singleplayer mode is active! Only one user can play.')
					text('Therefore, logging you in.')
				self.set_secure_cookie(
					'aetersum' + options.version.aetersum_version +\
					'-lunamalis' + \
					options.version.lunamalis_version + '-' + \
					str(config.port),
					str(0)
				)
				with tag('script'):
					text('window.location.replace("saves");')  # TEMP: 
			else:
				with tag('h2'):
					text('Login or Register')
				with tag('form', id="loginForm", method="POST"):
					with tag('h3'):
						text('Login')
					with tag('div', klass="input"):
						with tag('label', id="loginUsernameLabel"):
							text('Username*')
						with tag('input', id="loginUsername", name="username", required="required"):
							text('')
					doc.stag('br')
					with tag('div', klass="input"):
						with tag('label', id="loginPasswordLabel"):
							text('Password*')
						with tag('input', id="loginPassword", type="password", name="pw", required="required"):
							text('')
					doc.stag('br')
					with tag('button', id="loginSubmit", klass="formSubmit", type="submit", name="login", value="1"):
						text('Login')
					with tag('p'):
						text('* Required')
			doc.asis(body.footer())
		write_end += doc.getvalue()
		self.write(html_handler.HTMLWrapper.main(write_end))
		self.finish()

	@tornado.web.asynchronous
	def post(self):
		write_end = html_handler.Head(page=self.page).main()
		doc, tag, text, line = yattag.Doc().ttl()
		with tag('body'):
			if config.singleplayer_mode:
				with tag('p'):
					text('POST Login not allowed when singleplayer mode active.')
				with tag('script'):
					text('window.location.replace("login");')
			else:
				# So they want to log in.
				# First, we need to sanitize.
				# Second, we need to check to see if the username exists in the
				# database.
				username = self.get_argument('username')
				password = self.get_argument('pw')
				connection = mysql_handler.connect()
				try:
					with connection.cursor() as cursor:
						cursor.execute(
							"""
								SELECT * FROM `user_primary`
								WHERE `user_name` = %s;
							""",
							(username)
						)
						user_data = cursor.fetchone()
						if user_data == None:
							with tag('p'):
								text('That username could not be found.')
						else:
							# The username is obviously correct and does not need
							# validation. However, we must validate the password.
							if passlib_pwd_context.verify(
								password,
								user_data['user_password']
							):
								# Create a temporary session id.
								# This will be put into the cookie.
								acceptable_sid = False
								user_tmp_session_id = None
								while not acceptable_sid:
									user_tmp_session_id = random.randint(0, 999999999)
									cursor.execute(
										"""
											SELECT `user_tmp_session_id` FROM `user_primary`
											WHERE `user_tmp_session_id` = %s
										""",
										(user_tmp_session_id)
									)
									sid_search_results = cursor.fetchone()
									if sid_search_results == None:
										acceptable_sid = True
									else:
										print('Collision between session ids, '
										'trying again. Id tried = {first}; id '
										'already in use = {second}'.format(
											first=user_tmp_session_id,
											second=sid_search_results
										))

								cursor.execute(
									"""
										UPDATE `user_primary`
										SET `user_tmp_session_id` = %s
										WHERE `user_id` = %s
									""",
									(
										user_tmp_session_id,
										user_data['user_id']
									)
								)
								connection.commit()
								# All is good, give the cookie.
								self.set_secure_cookie(
									'aetersum' + options.version.aetersum_version +\
									'-lunamalis' + \
									options.version.lunamalis_version + '-' + \
									str(config.port),
									str(user_tmp_session_id)
								)
								with tag('p'):
									text('We have given you a cookie and you should\
									now be logged in. Click ')
									with tag('a', href="play"):
										text('here')
									text(' to play.')
							else:
								with tag('p'):
									with tag('a', href="login"):
										text('Password incorrect.')
				finally:
					connection.close()
		write_end += doc.getvalue()
		self.write(html_handler.HTMLWrapper.main(write_end))
		self.finish()

def main():
	# TODO: Print some debug information if called directly?
	pass

if __name__ == '__main__':
	main()
