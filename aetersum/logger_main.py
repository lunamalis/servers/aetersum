#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import logging
import os
import datetime
import config

def make_logger():
	# Rename the log file.
	cwd = os.getcwd()
	latest_log = os.path.join(cwd, 'aetersum', 'logs', 'latest.log')
	today = datetime.datetime.today()
	# OPTIMIZE: Three possible calls to a file?
	try:
		last_done = None
		with open(latest_log, 'r') as latest_log_f:
			last_done = latest_log_f.read().split('\n', 1)[0]
		os.rename(
			latest_log,
			os.path.join(cwd, 'aetersum', 'logs',str(last_done)+'.log')
		)
	except IOError:
		print('No log file at {file_loc}. A new one will be created.'.format(file_loc=latest_log))
		if not os.path.isdir(os.path.join(cwd, 'aetersum', 'logs')):
			os.makedirs(os.path.join(cwd, 'aetersum', 'logs'))

	with open(latest_log, 'w') as latest_log_f:
		latest_log_f.write(
			str(today.year) + '-' + str(today.month) + '-' + str(today.day) + \
			'_' + str(today.hour) + '_' + str(today.minute) + '_' + \
			str(today.second) + '_' + str(today.microsecond) + '\n'
		)

	logger = logging.getLogger('main_logger')
	logger.setLevel(logging.INFO)
	# logger_fh is used for the file.
	logger_fh = logging.FileHandler('aetersum'+os.sep+'logs'+os.sep+'latest.log')
	logger_fh.setLevel(logging.INFO)
	# logger_ch is used for the console.
	logger_ch = logging.StreamHandler()
	logger_ch.setLevel(logging.DEBUG)
	# create formatter and add it to the handlers
	logger_fh_formatter_s = '%(relativeCreated)6d [%(threadName)s/%(levelname)s] %(message)s'
	logger_ch_formatter_s = '%(relativeCreated)6d [%(threadName)s/%(log_color)s%(levelname)s%(reset)s] %(message)s'
	# @TODO
	logger_fh_formatter = logging.Formatter(logger_fh_formatter_s)
	logger_ch_formatter = None
	if config.enable_terminal_colors:
		# ColoredFormatter = importlib.import_module('colorlog.ColoredFormatter')
		from colorlog import ColoredFormatter
		logger_ch_formatter = ColoredFormatter(
			logger_ch_formatter_s,
			datefmt=None,
			reset=True,
			log_colors={
				'DEBUG':    'cyan',
				'INFO':     'white',
				'WARNING':  'yellow',
				'ERROR':    'red',
				'CRITICAL': 'red,bold',
			}
		)
	else:
		logger_ch_formatter = logging.Formatter(logger_ch_formatter_s)
	logger_fh.setFormatter(logger_fh_formatter)
	logger_ch.setFormatter(logger_ch_formatter)
	# add the handlers to the logger
	logger.addHandler(logger_fh)
	logger.addHandler(logger_ch)
	return logger

def main():
	return make_logger()

if __name__ == '__main__':
	logger = main()
	logger.info('This is an info!')
	logger.warning('This is a warning!')
	logger.error('This is an error!')
	logger.critical('This is a critical error!')
	logger.debug('This is a debug message.')
