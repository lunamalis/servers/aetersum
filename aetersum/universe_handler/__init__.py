#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from universe_handler import universe_generator
from universe_handler import universe

def generate(**kwargs):
	print(kwargs)
	universe_thing = create(universe_generator.generate(**kwargs))
	return universe_thing

def create(universe_data):
	return universe.Universe(universe_data=universe_data)

def main():
	pass

if __name__ == '__main__':
	main()
