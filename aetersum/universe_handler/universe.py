#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import math

class Universe():
	def __init__(self, universe_data):
		self.univ = universe_data
	def get_data(self):
		return json.dumps(self.univ)

	def get_system(self, system):
		return self.univ['systems'][system.get('id', -1)]

	def get_systems_near(self, **kwargs):
		x = kwargs.get('x', 0)
		y = kwargs.get('y', 0)
		dist = kwargs.get('dist', 100)
		systems = []
		for system in self.univ['systems']:
			system_try = self.get_system(system)
			true_dist = math.sqrt(
				(system_try.get('point', 0)[0] - x)**2
				+
				(system_try.get('point', 0)[1] - y)**2
			)
			if true_dist < dist:
				systems.append(system_try)
		return systems

	def get_closest_systems(self, **kwargs):
		'''Gets the closest systems to a particular system.'''
		x = kwargs.get('x', 0)
		y = kwargs.get('y', 0)
		num = kwargs.get('num', len(self.univ['systems']))
		close_systems = []
		systems_return = []

		for system in self.univ['systems']:
			system_try = self.get_system(system)
			true_dist = math.sqrt(
				(system_try.get('point', 0)[0] - x)**2
				+
				(system_try.get('point', 0)[1] - y)**2
			)
			if len(close_systems) > num:
				# Then we better start checking to see if this system is closer
				# than any of the others in the list.
				# This tricky loop is for helping remove it from the list. We do
				# not want to create a copy. See http://web.archive.org/web/20180717185056/https://stackoverflow.com/questions/6022764/python-removing-list-element-while-iterating-over-list/6024599
				for system_dist_try_i in range(len(close_systems) - 1, -1, -1):
					system_dist_try = close_systems[system_dist_try_i]
					if true_dist < system_dist_try[0]:
						# This system is closer, so remove that one from the
						# list and put this one in.
						del close_systems[system_dist_try_i]
						close_systems.append([true_dist, system_try['id']])

			else:
				close_systems.append([true_dist, system_try['id']])

		# Return only the systems; not the rarely used distance. That can be
		# extrapolated from the coordinates, which are given anyway.
		systems_return = []
		for close_system in close_systems:
			systems_return.append(self.univ['systems'][close_system[1]])
		return systems_return

	def get_current_system_id(self, fleet=0):
		return self.univ['fleets'][fleet]['currentSystemID']

	def get_current_system(self, fleet=0):
		return self.univ['systems'][self.get_current_system_id(fleet)]

	def travel_to_system_allowed(self, first_system, second_system):
		print('First system:', first_system)
		print('Second system:', second_system)
		first_system_hyperlanes = self.univ['systems'][first_system]['hyperlanes']
		second_system_hyperlanes = self.univ['systems'][second_system]['hyperlanes']
		if (self.univ['systems'][second_system]['id'] in first_system_hyperlanes) \
		or (self.univ['systems'][first_system]['id'] in second_system_hyperlanes):
			return True
		return False

	def travel_to_system(self, **kwargs):
		fleet_id = kwargs.get('fleet_id', 0)
		progress = kwargs.get('progress', 0)
		to_system_id = kwargs.get('to_system', 0)
		from_system_id = kwargs.get('from_system', 0)

		fleet = self.univ['fleets'][fleet_id]
		to_system = self.univ['systems'][to_system_id]
		from_system = self.univ['systems'][from_system_id]

		# First, we need to calculate the distance between the to_system and the
		# from_system.
		true_dist = math.sqrt(
			(to_system.get('point', 0)[0] - from_system.get('point', 0)[0])**2
			+
			(to_system.get('point', 0)[1] - from_system.get('point', 0)[1])**2
		)

		# Then, we calculate travel time.
		# TODO: Do
		# pseudocode:
		# get_fleet_mothership_faster_than_light_drive_speed
		# use that with true_dist
		# find time of arrival
		#
		# For now, we just instantly move them, it's easier.
		fleet['currentSystemID'] = to_system['id']
		# self.univ['fleets'][fleet_id] = fleet


	def tp_to_system(self, **kwargs):
		fleet_arg = kwargs.get('fleet', 0)
		to_system_id = kwargs.get('to_system', 0)
		fleet = self.univ['fleets'][fleet_arg]
		to_system = self.univ['systems'][to_system_id]
		fleet['currentSystemID'] = to_system['id']



def main():
	pass

if __name__ == '__main__':
	main()
