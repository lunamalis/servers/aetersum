#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

settings = {
	'static_path': os.path.join(os.getcwd(), 'aetersum', 'static')
}
