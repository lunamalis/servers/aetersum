#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yattag
class Head:
	def __init__(self, **kwargs):
		self.page = kwargs['page']
		self.title = kwargs.get('title', 'Untitled?')
		self.extra_scripts = kwargs.get('extra_scripts', [])
	def main(self):
		doc, tag, text, line = yattag.Doc().ttl()
		doc.asis('<!DOCTYPE html>')
		with tag('head'):
			doc.stag('meta', charset="utf-8")
			with tag('title'):
				text(self.title)
			with tag('link', href="css/main", rel="stylesheet"):
				text('')
			with tag('script'):
				text('window.lunamalis = {{c: {{page: {page}}}}}'.format(
					page=self.page
				))
			with tag('script', defer="", src="js/phaser"):
				text('')
			with tag('script', defer="", src="js/main"):
				text('')
			for extra_script in self.extra_scripts:
				with tag('script', defer="", src=extra_script):
					text('')

		return doc.getvalue()

class Body:
	def main(self):
		doc, tag, text, line = yattag.Doc().ttl()
		with tag('body'):
			text('')
		return doc.getvalue()
	def header(self):
		doc, tag, text, line = yattag.Doc().ttl()
		with tag('header'):
			with tag('h1'):
				text('Lunamalis')
		return doc.getvalue()

	def footer(self):
		doc, tag, text, line = yattag.Doc().ttl()
		with tag('footer'):
			# TODO: Content
			text('')
		return doc.getvalue()

class HTMLWrapper:
	def main(html_text):
		doc, tag, text = yattag.Doc().tagtext()
		doc.asis('<!DOCTYPE html>')
		with tag('html'):
			doc.asis(html_text)
		return doc.getvalue()

def main():
	pass

if __name__ == '__main__':
	main()
