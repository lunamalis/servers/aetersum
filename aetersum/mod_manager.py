#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import copy
import toml


class ModManager:
	def __init__(self, logger, shutdown, **kwargs):
		self.logger = logger
		self.shutdown = shutdown
		# This is negative one at first, as when the vanilla content loads this
		# is incremented, but the vanilla content is not a mod.
		self.loaded_mods = -1
		self.lang = kwargs.get('lang', 'en_US_POSIX')
		self.mod_dir = os.path.join(
			os.path.dirname(os.path.realpath(__file__)),
			'mods'
		)
		# List of possible classes for each file.
		self.classes = {
			'none': [],
			'system': [],
			'entity': [],
			'build': [],
			'faction_personality': [],
			'planet': [],
			'faction': [],
			'prescripted_system': [],
			'species': []
		}
		self.registry = {
			'keys': []
		}
		# Localization. Holds keys that point to a file containing their data.
		# A copy of self.classes, but not a reference.
		self.local = copy.deepcopy(self.classes)
		self.required = {
			'manifest': [
				'name',
				'key',
				'version'
			]
		}

	def list_mods(self):
		mods_list = []
		mod_dirs = [
			d for d in os.listdir(self.mod_dir) \
			if os.path.isdir(os.path.join(self.mod_dir, d))
		]
		for mod_dir in mod_dirs:
			if mod_dir == 'Lunamalis':
				self.logger.error('Can not load Lunamalis twice! Quitting!')
				self.shutdown()
				break
			if os.path.isfile(os.path.join(self.mod_dir, mod_dir, 'manifest.toml')):
				mods_list.append(mod_dir)
		self.mods_list = mods_list

	def load_vanilla(self):
		vanilla = self.load_mod(os.path.join(
			os.path.dirname(os.path.realpath(__file__)),
			'data' + os.sep + 'Lunamalis'
		))
		if vanilla == -1:
			return -1

	def load_all(self):
		'''
		Loads all the mods. Does not return anything.
		'''
		self.logger.info('Loading all mods.')
		# Loads the vanilla version first.
		vanilla = self.load_vanilla()
		if vanilla == -1:
			self.logger.info('Stopping loading of mods. (Error -1)')
			return -1

		# Find all the mods in the mods directory.
		self.list_mods()
		for mod_load in self.mods_list:
			self.logger.info('Loading mod {}'.format(mod_load))
			mod_result = self.load_mod(os.path.join(self.mod_dir, mod_load))
			if mod_result == -1:
				# It wants us to stop the loop!
				self.logger.info('Stopping loading of mods. (Error -1)')
				break
			self.logger.info('Loaded mod {}'.format(mod_load))
		self.logger.info('Loaded all {} mods.'.format(self.loaded_mods))

	def load_mod(self, target):
		'''Target expects a path.'''
		manifest = self.read_toml(target=target+os.sep+'manifest.toml')
		if manifest == -1:
			return -1
		if self.check_required(
			object_given=manifest,
			required=self.required['manifest'],
			target=target+os.sep+'manifest.toml'
		) == -1:
			return -1

		# Add to the number of loaded mods.
		self.loaded_mods += 1

		# Now, we need to read the common folder.
		mod_common_dir = target + os.sep + 'common'
		mod_common_files = []
		if os.path.isdir(mod_common_dir):
			# Loop through the dir and find all toml files.

			for root, dirs, files in os.walk(mod_common_dir):
				for file_toml in files:
					if os.path.splitext(file_toml)[1] == '.toml':
						mod_common_files.append(os.path.join(root, file_toml))

		# Now, we need to loop through all of those files and categorize them
		# by class.
		for mod_common_file in mod_common_files:
			file_toml = None
			with open(mod_common_file, 'r') as f:
				try:
					file_toml = toml.loads(f.read())
				except toml.TomlDecodeError as e:
					self.logger.error(
						'Quitting: TOML ValueError at file "{}":\n        {}'
						.format(mod_common_file, e)
					)
					self.shutdown()
					return -1

			# Check for required attributes, first.
			if self.check_required(
				object_given=file_toml,
				required=[
					'class'
				],
				target=mod_common_file
			) == -1:
				self.shutdown()
				return -1

			if file_toml['class'] not in self.classes:
				self.logger.error(
					'Class "{}" not in self.classes.'.format(file_toml['class'])
				)
				self.shutdown()
				return -1
			else:
				self.classes[file_toml['class']].append([
					manifest['key'],
					mod_common_file
				])
				# Append the key to the registry as well, so it can be lookup for
				# localization later.
				self.registry['keys'].append([
					manifest['key'] + '_' + file_toml['class'] + '_' + file_toml['key'],
					mod_common_file
				])

		# Now, we need to load all of the localization files.
		mod_local_dir = target + os.sep + 'localization' + os.sep + self.lang
		mod_local_files = []
		if os.path.isdir(mod_local_dir):
			# Load all class files.
			for class_type in self.classes:
				class_file_loc = os.path.join(
					mod_local_dir,
					class_type + '.toml'
				)
				if os.path.isfile(class_file_loc):
					# self.classes[class_type].append([
					# 	manifest['key'],
					# 	mod_local_dir + os.sep + class_type + '.toml'
					# ])
					# So, it is a file.
					# Now, we need to put it in local.
					# Open it first, but discard it, just to check the syntax.
					if self.read_toml(class_file_loc) == -1:
						return -1
					self.local[class_type].append([
						manifest['key'],
						class_file_loc
					])

		# Now, we loop through all of those files.

	# Some other possible configuration formats could be HOCON or Hjson, or even
	# JSON, but JSON is... not a good choice for configuration files.
	def read_toml(self, target):
		returner = None
		try:
			with open(target, 'r') as f:
				returner = toml.loads(f.read())
		except ValueError as e:
			self.logger.error(
				'Quitting: toml ValueError at file "{}":\n        {}'
				.format(mod_common_file, e)
			)
			self.shutdown()
			return -1
		return returner

	def check_required(self, object_given, required, target):
		'''
		Checks to see if a dictionary has all of the required fields or not.
		'''
		for required_arg in required:
			if required_arg not in object_given:
				return self.check_required_key_error(required_arg, target)
		return True

	def check_required_key_error(self, key, target):
		self.logger.error('Quitting: Key "{}" required in "{}".'.format(key, target))
		self.shutdown()
		return -1

	def get_class(self, request, mod=False):
		'''Opens all of the class files.'''
		request_return = {}
		for class_item in self.classes[request]:
			if mod:
				if class_item[0] != mod:
					# Forget this iteration of the for loop, and start the next
					# iteration.
					continue
			class_item_data = self.read_toml(target=class_item[1])

			request_return.update({
				class_item[0] + '_' + request + '_' + class_item_data['key']: \
				class_item_data
			})
		return request_return

	def get_freq(self, classes):
		return_array = []
		for class_k, class_v in classes.items():
			return_array.extend(
				[class_k] * class_v.get('spawn_odd', 1)
			)
		return return_array

	def get_local_class(self, request, mod=False):
		'''Gets the locals for a given class.'''
		request_return = {}
		for file_loc in self.local[request]:
			if mod:
				if file_loc[0] != mod:
					continue
			file_loc_data = self.read_toml(target=class_item[1])
			for loc_data_k, loc_data_v in file_loc_data.items():
				request_return.update({
					file_loc[0] + '_' + loc_data_k: loc_data_v
				})
			request_return.update({})
		return request_return
