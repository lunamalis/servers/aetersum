#!/usr/bin/env bash

# In the future, should variables be used here in this bash script, it is highly
# recommended to read https://web.archive.org/web/20180712231057/https://unix.stackexchange.com/questions/454694/how-can-i-harden-bash-scripts-against-causing-harm-when-changed-in-the-future
# first.

# ulimit limits how much memory a script is allowed to use.
# This is a dirty @HACK acting as a @TEMP fix to a mysterious memory leak that
# took around 12.5 GB of RAM after I left my computer for a few hours while it
# ran. A solution is in the works.
# 1000000 kb is 1 gb.
ulimit -v 1000000 && pipenv run python3.5 aetersum/start.py
